## simpplr-calendar

My response to the **Code Exercise for JS/React Developer** supplied by Simpplr.

The source code contains many comments.

Feel free to contact me directly at simon.robertson.2012@gmail.com or simon.computech@gmail.com

## development reasoning

There were various approaches available to the development of this calendar app but I ultimately went for something that demonstrated a typical route that I would take when developing a larger scale app.

The following packages were used for this app:

### react
https://www.npmjs.com/package/react

The standard for modern web based app development.

### redux
https://www.npmjs.com/package/redux

Provides a standard way to wire state into an app. It also provides a mechanism for dispatching actions within an app, which are generally used to update state. Middleware can also be created to intercept actions and provide additional logic behind the scenes.

### react-redux
https://www.npmjs.com/package/react-redux

Provides a bridge between React and Redux.

### recompose
https://www.npmjs.com/package/recompose

Provides a standard way to develop a React app using modern functional programming paradigms, and works very well when using TypeScript in a project. React components are constructed from one or more functions (higher-order components) instead of extending the Component and PureComponent classes provided by React.

### prop-types
https://www.npmjs.com/package/prop-types

The standard tool for enforcing React component prop types.

## notes

1. I decided to use a URL hash instead of the History API because the latter would have required server-side scripting (or the use of htaccess etc).

2. The intentional use of CSS Grid means the app will not render correctly in Internet Explorer.

3. I did add a button that reset the date to the current date, but this was removed in order to stick to the specification. It was also very tempting to allow individual days to be selected, but again I decided to stick to the specification.
