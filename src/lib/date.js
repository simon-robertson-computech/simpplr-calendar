//
const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

// Returns a string in the YYYY-MM-DD format from the provided date.
const toString = (d) => {
  let yn = d.getFullYear()
  let mn = d.getMonth() + 1
  let dn = d.getDate()

  let ys = yn.toString()
  let ms = mn.toString()
  let ds = dn.toString()

  if (ms.length === 1) {
    ms = '0' + ms
  }

  if (ds.legth === 1) {
    ds = '0' + ds
  }

  return ys + '-' + ms + '-' + ds
}

// Returns today's date.
export const today = () => {
  return toString(new Date())
}

// Validates a date.
export const validate = (date) => {
  const d = new Date(date)
  return Number.isNaN(d.getTime()) === false
}

// Returns the year from a date.
export const getYear = (date) => {
  return Number.parseInt(date.substr(0, 4), 10)
}

// Returns the month from a date.
export const getMonth = (date) => {
  return Number.parseInt(date.substr(5, 2), 10) - 1
}

// Returns the day from a date.
export const getDay = (date) => {
  return Number.parseInt(date.substr(8, 2), 10)
}

// Sets the month in a date, and returns the result.
export const setMonth = (date, value) => {
  const d = new Date(date)

  d.setMonth(Number.parseInt(value, 10))

  return toString(d)
}

// Sets the year in a date, and returns the result.
export const setYear = (date, value) => {
  const d = new Date(date)

  d.setFullYear(Number.parseInt(value, 10))

  return toString(d)
}

// Adds months to (or subtract months from) a date, and returns the result.
export const addMonths = (date, value) => {
  const d = new Date(date)

  d.setMonth(d.getMonth() + value)

  return toString(d)
}

// Adds years to (or subtract years from) a date, and returns the result.
export const addYears = (date, value) => {
  const d = new Date(date)

  d.setFullYear(d.getFullYear() + value)

  return toString(d)
}

// Returns a list of year metadata, suitable for use in the Selector component.
export const listYears = (date) => {
  let year = getYear(date)
  let list = []

  for (let i = 2000, n = 2040; i <= n; i ++) {
    list.push({
      value: i,
      selected: i === year
    })
  }

  return list
}

// Returns a list of month metadata, suitable for use in the Selector component.
export const listMonths = (date) => {
  let month = getMonth(date)
  let list = []

  for (let i = 0, n = 12; i < n; i ++) {
    list.push({
      value: i,
      label: monthNames[i],
      selected: i === month
    })
  }

  return list
}

// Returns a list of day metadata, suitable for use in the CalendarGrid component.
export const listDays = (date) => {
  const a = new Date(date)
  const b = new Date(date)
  const c = new Date()

  // Zero the time to make date comparisions easier.
  a.setHours(0, 0, 0, 0)
  b.setHours(0, 0, 0, 0)
  c.setHours(0, 0, 0, 0)

  // Step back to the start of the current month.
  a.setDate(1)

  // If the current day is not Monday, step back to Monday.
  if (a.getDay() !== 1) {
    if (a.getDay() === 0) {
      a.setDate(a.getDate() - 6)
    } else {
      a.setDate(a.getDate() - (a.getDay() - 1))
    }
  }

  let list = []

  for (let i = 0, n = 42; i < n; i ++) {
    list.push({
      value: (a.getDate() < 10 ? '0' : '') + a.getDate(),
      outside: a.getMonth() !== b.getMonth(),
      selected: a.getTime() === c.getTime(),
      timestamp: a.getTime()
    })

    a.setDate(a.getDate() + 1)
  }

  return list
}
