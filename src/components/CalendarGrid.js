import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

import { listDays } from '../lib/date'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    date: PropTypes.string.isRequired,
    onDateChange: PropTypes.func.isRequired
  })
)

//
export const CalendarGrid = enhance((props) => {
  // Create the date "cell" elements for the calendar grid.
  const cells = listDays(props.date).map((item) => {
    let className = 'cell date'

    if (item.outside) {
      className += ' outside'
    }

    if (item.selected) {
      className += ' selected'
    }

    return (
      <div className={className} key={item.timestamp}>
        {item.value}
      </div>
    )
  })

  return (
    <div className="CalendarGrid">
      <div className="container">
        <div className="cell head">Mo</div>
        <div className="cell head">Tu</div>
        <div className="cell head">We</div>
        <div className="cell head">Th</div>
        <div className="cell head">Fr</div>
        <div className="cell head">Sa</div>
        <div className="cell head">Su</div>

        {cells}
      </div>
    </div>
  )
})
