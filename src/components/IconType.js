// An enum of supported material icon types.
export const IconType = {
  ARROW_LEFT: 'arrow_left',
  ARROW_RIGHT: 'arrow_right',
  EVENT: 'event',
  SKIP_NEXT: 'skip_next',
  SKIP_PREVIOUS: 'skip_previous'
}
