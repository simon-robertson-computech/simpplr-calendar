import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  }),
  // Define callbacks that can be used within the component.
  Recompose.withHandlers({
    // Called when an item in the drop-down list is selected.
    handleChange: (props) => (event) => {
      props.onChange(event.target.value)
    }
  })
)

//
export const Selector = enhance((props) => {
  let selected = ''

  const options = props.options.map((option) => {
    if (option.selected) {
      selected = option.value
    }

    return (
      <option key={option.value} value={option.value}>
        {option.label ? option.label : option.value}
      </option>
    )
  })

  return (
    <select className="Selector" value={selected} onChange={props.handleChange}>
      {options}
    </select>
  )
})
