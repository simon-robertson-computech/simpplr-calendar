import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

import { CalendarControls } from './CalendarControls'
import { CalendarGrid } from './CalendarGrid'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    date: PropTypes.string.isRequired,
    onDateChange: PropTypes.func.isRequired
  })
)

//
export const Calendar = enhance((props) => {
  return (
    <div className="Calendar">
      <CalendarControls date={props.date} onDateChange={props.onDateChange} />
      <CalendarGrid date={props.date} onDateChange={props.onDateChange} />
    </div>
  )
})
