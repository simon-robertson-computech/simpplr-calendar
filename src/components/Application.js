import * as React from 'react'
import * as Redux from 'react-redux'
import * as Recompose from 'recompose'

import { Calendar } from './Calendar'

import { actions } from '../redux'

// Create the higher-order components.
const enhance = Recompose.compose(
  Redux.connect(
    // Maps redux state to props.
    (state) => ({
      // The current/selected date.
      calendarDate: state.calendarDate
    }),
    // Maps redux action creators to props.
    (dispatch) => ({
      // Updates the current/selected date.
      changeDate: (date) => dispatch(actions.changeDate(date))
    })
  )
)

// The main application component.
export const Application = enhance((props) => {
  return (
    <div className="Application">
      <Calendar date={props.calendarDate} onDateChange={props.changeDate} />
    </div>
  )
})
