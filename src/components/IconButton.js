import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

import { Icon } from './Icon'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    type: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
  }),
  // Define callbacks that can be used within the component.
  Recompose.withHandlers({
    // Called when the component is clicked.
    handleClick: (props) => (event) => {
      event.stopPropagation()
      props.onPress()
    }
  })
)

//
export const IconButton = enhance((props) => {
  return (
    <div className="IconButton" onClick={props.handleClick}>
      <Icon type={props.type} />
    </div>
  )
})
