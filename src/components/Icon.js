import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    type: PropTypes.string.isRequired
  })
)

//
export const Icon = enhance((props) => {
  return (
    <div className="material-icons Icon">
      {props.type}
    </div>
  )
})
