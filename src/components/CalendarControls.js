import * as React from 'react'
import * as Recompose from 'recompose'
import * as PropTypes from 'prop-types'

import { IconButton } from './IconButton'
import { IconType } from './IconType'
import { Selector } from './Selector'

import { addMonths, listMonths, listYears, setMonth, setYear, today } from '../lib/date'

// Create the higher-order components.
const enhance = Recompose.compose(
  // Enforce prop types. These props are passed into the component.
  Recompose.setPropTypes({
    date: PropTypes.string.isRequired,
    onDateChange: PropTypes.func.isRequired
  }),
  // Define callbacks that can be used within the component.
  Recompose.withHandlers({
    // Called when a month is selected from the drop-down list.
    handleMonthChange: (props) => (value) => {
      props.onDateChange(setMonth(props.date, value))
    },
    // Called when a year is selected from the drop-down list.
    handleYearChange: (props) => (value) => {
      props.onDateChange(setYear(props.date, value))
    },
    // Called when the "next" icon button is pressed.
    handleNextButtonPress: (props) => () => {
      props.onDateChange(addMonths(props.date, 1))
    },
    // Called when the "previous" icon button is pressed.
    handlePrevButtonPress: (props) => () => {
      props.onDateChange(addMonths(props.date, -1))
    }
  })
)

//
export const CalendarControls = enhance((props) => {
  const months = listMonths(props.date)
  const years = listYears(props.date)

  return (
    <div className="CalendarControls">
      <IconButton type={IconType.ARROW_LEFT} onPress={props.handlePrevButtonPress} />
      <div className="container">
        <Selector options={months} onChange={props.handleMonthChange} />
        <Selector options={years} onChange={props.handleYearChange} />
      </div>
      <IconButton type={IconType.ARROW_RIGHT} onPress={props.handleNextButtonPress} />
    </div>
  )
})
