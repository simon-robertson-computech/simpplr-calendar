import * as Redux from 'redux'

import * as actions from './actions'
import * as middleware from './middleware'
import * as reducers from './reducers'

// Create the redux store.
const store = Redux.createStore(
  Redux.combineReducers(reducers),
  Redux.applyMiddleware(middleware.router)
)

export { actions, store }
