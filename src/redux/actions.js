// An enum of action types used in the application.
export const ActionType = {
  DATE_CHANGE: 'DATE_CHANGE'
}

// Updates application state with a selected date.
export const changeDate = (date) => ({
  type: ActionType.DATE_CHANGE, payload: { date }
})
