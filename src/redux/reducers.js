import { ActionType } from './actions'

// Holds the selected date.
export const calendarDate = (state = null, action) => {
  if (action.type === ActionType.DATE_CHANGE) {
    return action.payload.date
  }

  return state
}
