import { ActionType } from './actions'
import { today, validate } from '../lib/date'

// Redux middleware that handles changes to the browser's URL hash.
export const router = (store) => {
  // Called whenever the URL hash has changed, or when it needs to be initialised.
  const update = () => {
    let date
    let dateMatch = window.location.hash.match(/^#?(\d\d\d\d-\d\d-\d\d)$/)

    if (dateMatch === null) {
      // Default to today's date, and replace the hash.
      date = today()
      window.location.replace('#' + date)
    } else {
      date = dateMatch[1]

      // Make sure we have a valid date.
      if (validate(date) === false) {
        // Default to today's date, and replace the hash.
        date = today()
        window.location.replace('#' + date)
      }
    }

    // Update the application state.
    store.dispatch({ type: 'DATE_CHANGE', payload: { date }, routerDispatched: true })
  }

  // Initialize the date hash.
  update()

  // Listen for hash changes.
  window.addEventListener('hashchange', update)

  return (next) => (action) => {
    // Make sure we are not handling an action that has been dispatched from this middleware.
    if (action.type === ActionType.DATE_CHANGE && action.routerDispatched !== true) {
      // Update the hash after making sure the date payload is valid.
      if (validate(action.payload.date)) {
        window.location.assign('#' + action.payload.date)
      } else {
        // Nuke the action so it does not reach the redux store.
        console.warn('Invalid date dispatched:', action.payload.date)
        return
      }
    }

    return next(action)
  }
}
