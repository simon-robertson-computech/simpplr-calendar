import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as Redux from 'react-redux'

import { Application } from './components/Application'
import { store } from './redux'

import './index.css'

// Application entry point.
const main = () => {
  ReactDOM.render(
    <Redux.Provider store={store}>
      <Application />
    </Redux.Provider>,
    document.querySelector('main')
  )
}

//
main()
